const CACHE_NAME = 'version-7';
const urlsToCache = [
  'index.html',
  '/offline.html',
  '/manifest.json',
  '/logo_app.png',
  '/static/js/bundle.js',
  '/static/css/main.css',
];

self.addEventListener('install', (event) => {
  console.log(CACHE_NAME + ' install');
  self.skipWaiting();

  event.waitUntil(
    caches.open(CACHE_NAME).then(async (cache) => {
      try {
        console.log('Opend Cache');
        return await cache.addAll(urlsToCache);
      } catch (e) {
        console.log(e);
      }
    }),
  );
});

self.addEventListener('activate', (event) => {
  const cacheWhiteList = [];
  clients.claim();
  console.log(CACHE_NAME + ' activate');
  cacheWhiteList.push(CACHE_NAME);
  event.waitUntil(
    caches.keys().then((cacheNames) =>
      Promise.all(
        cacheNames.map((cacheName) => {
          if (!cacheWhiteList.includes(cacheName)) {
            return caches.delete(cacheName);
          }
        }),
      ),
    ),
  );
});

function fetchNavigate(event) {
  return event.respondWith(
    (async () => {
      try {
        const preloadResponse = await event.preloadResponse;
        if (preloadResponse) {
          return preloadResponse;
        }
        const response = await fetch(event.request.clone());
        if (response) {
          const cache = await caches.open(CACHE_NAME);
          await cache.put(event.request, response.clone());
        }
        return response;
      } catch (e) {
        const cache = await caches.open(CACHE_NAME);
        const res = await cache.match(event.request);
        if (res) {
          return res;
        } else {
          return cache.match('/offline.html').then((response) => {
            if (response) {
              return response;
            }
          });
        }
      }
    })(),
  );
}

async function getFromCache(event) {
  const cache = await caches.open(CACHE_NAME);
  return await cache.match(event.request.clone());
}

function fetchNoCors(event) {
  return event.respondWith(
    (async () => {
      try {
        const res = await getFromCache(event);
        if (res) {
          return res;
        } else {
          const response = await fetch(event.request.clone());
          const cache = await caches.open(CACHE_NAME);
          if (response.status === 200) {
            await cache.put(event.request, response.clone());
            return response;
          } else {
            return 'no data';
          }
        }
      } catch (e) {
        console.log(event);
      }
    })(),
  );
}

async function fetchCors(event) {
  try {
    return event.respondWith(
      (async () => {
        try {
          const response = await fetch(event.request.clone());
          if (event.request.method === 'GET' && response.status === 200) {
            const cache = await caches.open(CACHE_NAME);
            await cache.put(event.request, response.clone());
          }
          return response;
        } catch (e) {
          const cache = await caches.open(CACHE_NAME);
          const res = await cache.match(event.request);
          if (res) {
            return res;
          }
        }
      })(),
    );
  } catch (e) {
    console.log(e);
  }
}

self.addEventListener('fetch', (event) => {
  if (!event.request.url.startsWith('http')) {
    return 0;
  }
  if (event.request.mode === 'navigate') {
    return fetchNavigate(event);
  } else if (event.request.mode === 'no-cors') {
    return fetchNoCors(event);
  } else {
    return fetchCors(event);
  }
});

self.addEventListener('push', function (event) {
  const data = event.data ? event.data.json() : {};
  event.waitUntil(
    self.registration.showNotification(data.title, {
      body: data.content,
      data: data,
    }),
  );
});

self.addEventListener('notificationclick', (event) => {
  console.log(event);
  event.notification.close();
  event.waitUntil(self.clients.openWindow(event.notification.data.url));
});
