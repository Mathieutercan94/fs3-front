# FS3-front

## Getting started

## Run

    clone le projet
    npm i --force
    npm run start

Le serveur React va être lancé sur http://localhost:3000

## deploy

    clone le projet
    npm i --force
    npm run build

Pour lancer le serveur de prod react en local, on peut le paquet serve
npm install --global serve
serve -s $nom_du_dossier_de_build -l $port_d'écoute
il sera joignable à l'adresse suivante http://localhost:$port

Sur le serveur distant, le build du front-end est hébergé par un serveur nginx
La pipeline Ci s'occupe de deployer au bon endroit sur le serveur distant
dans le cas présent, dans le dossier /var/www/build
