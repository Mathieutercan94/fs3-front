exports.en = {
  0: 'Wrong JWT.',

  1: 'Unknown user.',

  2: 'Banned account. Please contact admin.',

  3: 'Email already in use.',

  4: 'Validation error...',

  5: 'Email or password wrong..',

  6: 'Remove missing.',

  9: 'Wrong URL.',

  10: 'URL is undefined.',

  11: 'Name is undefined.',

  13: 'Airport is missing in the body.',

  14: 'Wrong body.',

  16: "This airport doesn't exist.",

  17: "This user doesn't exist.",

  18: 'Internal error.',

  19: 'Argument missing in form.',

  20: 'Query missing.',

  21: 'User has no data.',

  22: 'UUID not found.',

  23: 'The number of users must be greater than 1.',

  24: 'A user has been added several times.',

  25: 'This user is not in this conversation.',

  26: 'This query is not an ID.',

  27: 'This type is not supported.',

  28: 'Query is not defined.',
};
