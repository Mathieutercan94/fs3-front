import { useEffect, useState } from 'react';

const MobileView = ({ children, component }) => {
  const [view, setView] = useState(null);

  function chooseComponent() {
    if (window.location.pathname !== '/') setView(children.find((l) => l.type.name === component));
    else setView(children[0]);
  }

  useEffect(() => {
    if (window.innerWidth >= 650) setView(children);
    else chooseComponent();
  }, [view]);

  window.addEventListener('resize', () =>
    window.innerWidth >= 650 ? setView(children) : chooseComponent(),
  );
  return view;
};

export default MobileView;
