import styled from 'styled-components';
import React, { useState } from 'react';
import { changeLang, i18n } from '../../i18n';

const Button = styled.button`
  border: 0;
  color: grey;
  background: transparent;
  &:hover {
    color: white;
    cursor: pointer;
  }
`;

const ButtonHome = styled.button`
  min-width: max(4vw, 40px);
  border: 0;
  background: rgb(58, 59, 60);
  color: grey;
  &:hover {
    color: white;
    cursor: pointer;
  }
`;

function TranslateButtonAuth({ value, onClick }) {
  return (
    <Button
      onClick={() => {
        onClick(value);
      }}
    >
      {value}
    </Button>
  );
}

function TranslateButtonHome({ value }) {
  const [selected, setSelected] = useState(i18n.language);

  return (
    <ButtonHome
      onClick={() => {
        setSelected(changeLang(value[(value.findIndex((e) => e === i18n.language) + 1) % 2]));
      }}
    >
      {selected}
    </ButtonHome>
  );
}

export { TranslateButtonAuth, TranslateButtonHome };
