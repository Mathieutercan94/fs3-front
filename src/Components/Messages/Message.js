import styled from 'styled-components';
import React from 'react';

const Main = styled.div`
  background: rgb(36, 37, 38);
  padding: 2.5px 1vw;
  color: white;
  width: fit-content;
  max-width: 50%;
  border-radius: 10px;
`;

function Message({ UserId, user, author, content }) {
  const styleMsg = {};
  if (UserId === user.id || author === user.id) {
    styleMsg.alignSelf = 'flex-end';
    styleMsg.color = 'white';
    styleMsg.background = 'rgb(0, 106, 255)';
  } else {
    styleMsg.alignSelf = 'flex-start';
  }

  return (
    <Main style={styleMsg}>
      <span>{content}</span>
    </Main>
  );
}

export default Message;
