import styled from 'styled-components';
import React, { useContext, useEffect, useState } from 'react';
import InputBar from '../Bars/InputBar';
import SearchBar from '../Bars/SearchBar';
import Axios from '../../Axios';
import { UserContext } from '../Providers/UserProvider';

const Main = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: 2;
`;

function New({ conv, socket }) {
  const { user } = useContext(UserContext);
  const [selectedProfile, setSelectedProfile] = useState(null);

  const createConvAndSendMsg = async (value) => {
    const existConv = conv.find((e) => e.id === selectedProfile.id);
    if (existConv === undefined) {
      const convs = await Axios.post('conversation/create', {
        users: [user.id, selectedProfile.id],
      });
      socket.emit('get_connected', (response) => {
        const index = response.findIndex((obj) => obj.userid === selectedProfile.id);
        if (index > -1) {
          socket.emit('send invitation', {
            to: response[index].socketId,
            id: convs.data.id,
            lastMessage: value.content,
            authors: [{ id: user.id, name: user.name, surname: user.surname }],
          });
        }
      });
      await Axios.post('conversation/sendMessage', {
        conversation: convs.data.id,
        message: value.content,
      });
      window.location.pathname = `/${convs.data.id}`;
    } else {
      window.location.pathname = `/${existConv.id}`;
    }
  };

  useEffect(() => {}, [selectedProfile]);

  return (
    <Main>
      <SearchBar chooseUser={setSelectedProfile} user={selectedProfile} />
      <InputBar postMessage={createConvAndSendMsg} />
    </Main>
  );
}

export default New;
