import styled from 'styled-components';
import React, { useState, useEffect, useContext } from 'react';
import Message from './Message';
import InputBar from '../Bars/InputBar';
import TopBar from '../Bars/TopBar';
import Axios from '../../Axios';
import { UserContext } from '../Providers/UserProvider';

const Main = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: 2;
`;

const Scrolling = styled.div`
  padding: 2vh 0;
  overflow-y: auto;
  flex: 1;
`;

const ListMsg = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 1vw;
  row-gap: 1vh;
`;

function ChatMessages({ messages }) {
  return <ListMsg>{[...messages]}</ListMsg>;
}

function Chat({ socket }) {
  const { user } = useContext(UserContext);
  const [messages, setMessages] = useState([]);
  const [selectedConv, setSelectedConv] = useState({});

  useEffect(() => {
    if (window.location.pathname !== '/') {
      Axios.get('conversation/get', { params: { id: window.location.pathname.substring(1) } }).then(
        (res) => {
          const tmp = res.data.messages.map((e) => (
            <Message user={user} content={e.content} author={e.UserId} key={e.createdAt} />
          ));
          setMessages(tmp);
          setSelectedConv(res.data.users[0]);
        },
      );
      const size = window.location.pathname.length;
      const id = window.location.pathname.substring(1, size);
      if (socket) {
        socket.emit('join room', id);
      }
    }
  }, []);

  useEffect(() => {
    if (socket) {
      socket.on(
        'chat message',
        (message) => {
          const tmp = [...messages];
          tmp.push(
            <Message
              content={message.content}
              key={message.createdAt}
              author={message.id}
              user={user}
            />,
          );
          setMessages([...tmp]);
        },
        [messages],
      );
    }
  });
  const postMessage = (value) => {
    Axios.post('conversation/sendMessage', {
      conversation: window.location.pathname?.substring(1),
      message: value.content,
    });
  };

  return (
    <Main>
      <TopBar dest={selectedConv} />
      <Scrolling>
        <ChatMessages messages={messages} />
      </Scrolling>
      <InputBar postMessage={postMessage} />
    </Main>
  );
}

export default Chat;
