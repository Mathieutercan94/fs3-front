import { Navigate } from 'react-router-dom';
import React, { useContext } from 'react';
import { AuthContext } from '../Providers/AuthProvider';

export function Private({ children }) {
  const { isAuth } = useContext(AuthContext);
  return isAuth ? children : <Navigate to='/login' />;
}

export function Protected({ children }) {
  const { isAuth } = useContext(AuthContext);
  return !isAuth ? children : <Navigate to='/' />;
}
