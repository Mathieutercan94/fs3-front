import React, { createContext, useMemo, useState } from 'react';
import Axios from '../../Axios';

const AuthContext = createContext('');

function AuthProvider({ children }) {
  const [isAuth, setIsAuth] = useState(window.localStorage.getItem('jwt'));

  const login = (e) => {
    Axios.post('auth/login', e)
      .then(async (res) => {
        window.localStorage.setItem('jwt', JSON.stringify(res.data.jwtToken));
        window.localStorage.setItem('user', JSON.stringify(res.data.profile));
        Axios.defaults.headers.common.Authorization = `Bearer ${res.data.jwtToken}`;
        setIsAuth(true);
        const response = await Axios.get('conversation/getall');
        window.location.pathname = window.innerWidth >= 650 ? `/${response.data[0]?.id}` : '/';
      })
      .catch((err) => console.error(err.stack));
  };

  const logout = () => {
    window.localStorage.removeItem('jwt');
    window.localStorage.removeItem('user');
    setIsAuth(false);
    window.location = '/';
  };

  const value = useMemo(() => ({ isAuth, login, logout }), [isAuth]);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export { AuthContext, AuthProvider };
