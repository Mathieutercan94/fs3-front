import React, { createContext, useMemo, useState } from 'react';

const UserContext = createContext({});

function UserProvider({ children }) {
  const [user, setUser] = useState(JSON.parse(window.localStorage.getItem('user')) || {});

  const modifyUser = (value) => {
    setUser(value);
  };

  const removeUser = () => {
    setUser({});
  };

  const value = useMemo(() => ({ user, modifyUser, removeUser }), [user]);

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
}

export { UserProvider, UserContext };
