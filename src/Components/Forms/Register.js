import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import React, { useState } from 'react';
import Axios from '../../Axios';

const Main = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: 2vh;
`;

const Input = styled.input`
  width: 100%;
  border-radius: 5px;
  padding-left: 15px;
  outline: 0;
  border: 0;
  height: 5vh;
  background: rgb(58, 59, 60);
  color: white;
  &:-webkit-autofill,
  :-webkit-autofill:active,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus {
    background: rgb(58, 59, 60) !important;
    -webkit-background: rgb(58, 59, 60) !important;
  }
`;

const Button = styled.button`
  display: flex;
  margin: auto;
  margin-top: 2vh;
  padding: 10px 2vw;
  border-radius: 50px;
  border: 0;
  background: rgb(0, 106, 255);
  color: white;
  &:hover {
    cursor: pointer;
  }
`;

function Register() {
  const { t } = useTranslation();
  // const [checkedPwd, setCheckedPwd] = useState(false);
  const [info, setInfo] = useState({
    name: '',
    surname: '',
    email: '',
    password: '',
  });

  const register = async () => {
    const res = await Axios.post('auth/register', info);
    window.localStorage.setItem('user', JSON.stringify(res.data.profile));
    window.localStorage.setItem('jwt', JSON.stringify(res.data.jwtToken));
    Axios.defaults.headers.common.Authorization = `Bearer ${res.data.jwtToken}`;
    window.location = '/';
  };

  // function checkPassword(e) {
  //  const psw = e.target.value;
  //  const regUpper = RegExp('/r+[A-Z]');
  //  const regLower = RegExp('/r+[a-z]');
  //  const regNumber = RegExp('/r+[0-9]');
  //  const regSpecial = RegExp('/r+w');
  //  const reg = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})');
  //  if (
  //    psw < 8 ||
  //    !psw.regex.match(regUpper) ||
  //    !psw.regex.match(regLower) ||
  //    !psw.regex.match(regNumber) ||
  //    !psw.regex.match(regSpecial)
  //  )
  //    return false;
  // }

  const fillInfo = (e) => {
    const tmp = info;
    tmp[e.target.id] = e.target.value;
    setInfo(tmp);
  };

  return (
    <Main>
      <Input onChange={fillInfo} id='email' required type='email' placeholder='Email' />
      <Input
        onChange={fillInfo}
        id='password'
        required
        type='password'
        placeholder={t('Password')}
        // checked={checkedPwd}
      />
      <Input id='confirm-password' required type='password' placeholder={t('confirm-password')} />
      <Input onChange={fillInfo} id='name' required type='text' placeholder={t('name')} />
      <Input onChange={fillInfo} id='surname' required type='text' placeholder={t('surname')} />
      <Button onClick={register}>{t('register')}</Button>
    </Main>
  );
}

export default Register;
