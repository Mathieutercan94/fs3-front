import styled from 'styled-components';
import { useTranslation } from 'react-i18next';
import React, { useContext, useState } from 'react';
import { AuthContext } from '../Providers/AuthProvider';

const Main = styled.div`
  width: 100%;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  row-gap: 2vh;
`;

const Input = styled.input`
  width: 100%;
  border-radius: 5px;
  padding-left: 15px;
  outline: 0;
  border: 0;
  height: 5vh;
  background: rgb(58, 59, 60);
  color: white;
  &:-webkit-autofill,
  :-webkit-autofill:active,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus {
    background: rgb(58, 59, 60) !important;
  }
`;

const Button = styled.button`
  width: max-content;
  padding: 10px 2vw;
  margin-top: 2vh;
  border-radius: 50px;
  border: 0;
  background: rgb(0, 106, 255);
  color: white;
  &:hover {
    cursor: pointer;
  }
`;

function Login() {
  const { login } = useContext(AuthContext);
  const [infos, setInfos] = useState({ email: '', password: '' });

  const onChange = (e) => {
    const tmp = infos;
    tmp[e.target.type] = e.target.value;
    setInfos(tmp);
  };
  const { t } = useTranslation();

  return (
    <Main>
      <Input onChange={onChange} required type='email' placeholder='Email' />
      <Input onChange={onChange} required type='password' placeholder={t('password')} />
      <Button onClick={() => login(infos)}>{t('connect')}</Button>
    </Main>
  );
}

export default Login;
