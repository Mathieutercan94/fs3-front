import styled from 'styled-components';
import React, { useEffect, useState } from 'react';
import MobileChannel from '../Channels/MobileChannel';
import Axios from '../../Axios';

const Main = styled.div`
  height: 5vh;
  display: flex;
  align-items: center;
  border-bottom: 1px solid grey;
  position: relative;
  flex-wrap: wrap;
  justify-content: flex-end;
`;

const Input = styled.input`
  height: calc(5vh - 2px);
  flex: 1;
  padding-left: 10px;
  background: rgb(36, 37, 38);
  border: 0;
  outline: none;
  color: white;
  &:-webkit-autofill,
  :-webkit-autofill:active,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus {
    background: rgb(58, 59, 60) !important;
    -webkit-background: rgb(58, 59, 60) !important;
  }
  font-size: large;
`;

const Span = styled.span`
  height: 5vh;
  flex: 1;
  color: white;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

const ListProfile = styled.div`
  width: calc(100% - 5vw);
  max-height: 15vh;
  background: rgb(36, 37, 38);
  display: flex;
  flex-direction: column;
`;

const Row = styled.span`
  display: flex;
  align-items: center;
  height: 5vh;
  color: white;
  border-top: 1px solid black;
  padding-left: 10px;
  &:hover {
    cursor: pointer;
    background: rgb(58, 59, 60);
  }
`;

const Button = styled.button`
  background: transparent;
  width: 3vw;
  color: rgb(58, 59, 60);
  border: 0;
  &:hover {
    cursor: pointer;
    color: white;
  }
`;

function SearchBar({ chooseUser, user }) {
  const [inputValue, setInputValue] = useState('');
  const [search, setSearch] = useState('');
  const [listSpan, setListSpan] = useState([]);

  const onChange = (e) => {
    setInputValue(e.target.value);
  };

  const selectRow = (e) => {
    chooseUser(e);
    setListSpan([]);
  };

  const deleteSelection = () => {
    setInputValue('');
    setListSpan([]);
    chooseUser(null);
  };

  useEffect(() => {
    const timer = setTimeout(() => setSearch(inputValue), 1000);
    return () => clearTimeout(timer);
  }, [inputValue]);

  useEffect(() => {
    if (search !== '') {
      Axios.get('profile/get', {
        params: {
          type: 'name',
          query: search,
        },
      }).then((res) => {
        if (res.data.length !== 0) {
          const tmp = res.data.map((e) => (
            <Row onClick={() => selectRow(e)}>{`${e.name} ${e.surname}`}</Row>
          ));
          setListSpan(tmp);
        } else setListSpan([]);
      });
    }
  }, [search]);

  return (
    <Main>
      <MobileChannel />
      {user !== null ? (
        <>
          <Span>{`${user.name} ${user.surname}`}</Span>
          <Button onClick={deleteSelection}>X</Button>
        </>
      ) : (
        <Input onChange={onChange} value={inputValue} placeholder='Find someone ...' />
      )}
      {listSpan.length !== 0 ? <ListProfile>{listSpan}</ListProfile> : ''}
    </Main>
  );
}

export default SearchBar;
