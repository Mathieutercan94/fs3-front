import styled from 'styled-components';
import React from 'react';
import MobileChannel from '../Channels/MobileChannel';

const Main = styled.div`
  height: 5vh;
  display: flex;
  padding-right: 1vw;
  align-items: center;
  border-bottom: 1px solid grey;
`;
const PersoInfo = styled.div`
  display: flex;
  flex: 1;
  column-gap: 1vw;
  justify-content: flex-end;
`;

function TopBar({ dest }) {
  return (
    <Main>
      <MobileChannel />
      <PersoInfo>
        <span>{dest.name ? `${dest.name} ${dest.surname}` : ''}</span>
      </PersoInfo>
    </Main>
  );
}

export default TopBar;
