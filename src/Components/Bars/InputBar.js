import styled from 'styled-components';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

const Main = styled.div`
  display: flex;
  justify-content: flex-end;
  min-height: 4vh;
  align-items: center;
  column-gap: 1vw;
  padding: 1vh 2vw;
  border-top: 1px solid grey;
`;

const Input = styled.textarea`
  width: 70%;
  min-height: 70%;
  outline: 0;
  border: 0;
  border-radius: 10px;
  word-break: break-word;
  resize: none;
  overflow-y: hidden;
  padding: 0 10px;
  background: rgb(36, 37, 38);
  color: white;
`;

const SendButton = styled.button`
  width: fit-content;
  border: 0;
  border-radius: 10px;
  background: rgb(36, 37, 38);
  color: white;
  padding: 5px 10px;
  &:hover {
    cursor: pointer;
    filter: brightness(1.4);
  }
`;

const useAutosizeTextArea = (textAreaRef, value) => {
  /* eslint-disable no-param-reassign */
  useEffect(() => {
    if (textAreaRef) {
      textAreaRef.style.height = '0px';
      const { scrollHeight } = textAreaRef;
      textAreaRef.style.height = `${scrollHeight}px`;
    }
  }, [textAreaRef, value]);
  /* eslint-enable no-param-reassign */
};

function InputBar({ postMessage }) {
  const { t } = useTranslation();
  const textAreaRef = useRef(null);
  const [value, setValue] = useState('');

  const handleChange = (e) => {
    if (e.key === 'Enter') return;
    const val = e.target.value;
    setValue(val);
  };

  useAutosizeTextArea(textAreaRef.current, value);

  const send = (e) => {
    if (value.trim().length !== 0) {
      postMessage({ content: value, author: 'me' });
      setValue('');
    }
    e.preventDefault();
  };

  return (
    <Main>
      <Input
        type='text'
        placeholder={t('input-placeholder')}
        ref={textAreaRef}
        onChange={handleChange}
        onKeyDown={(e) => (e.key === 'Enter' ? send(e) : null)}
        row={1}
        value={value}
      />
      <SendButton onClick={send}>{t('send')}</SendButton>
    </Main>
  );
}

export default InputBar;
