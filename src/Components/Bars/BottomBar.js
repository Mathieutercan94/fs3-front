import styled from 'styled-components';
import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faRightFromBracket } from '@fortawesome/free-solid-svg-icons';
import { AuthContext } from '../Providers/AuthProvider';
import { UserContext } from '../Providers/UserProvider';
import { TranslateButtonHome } from '../Utils/Button';

const Main = styled.div`
  display: flex;
  justify-content: flex-end;
  align-self: center;
  height: 6vh;
  border-top: 1px solid grey;
  width: 100%;
`;

const Button = styled.button`
  color: white;
  width: 15%;
  border: 0;
  background: rgb(24, 25, 26);
  &:hover {
    cursor: pointer;
    filter: brightness(2);
  }
`;

const Span = styled.span`
  flex: 1;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
`;

function BottomBar() {
  const { logout } = useContext(AuthContext);
  const { user, removeUser } = useContext(UserContext);

  const addChannel = () => {
    window.location.pathname = '/new';
  };

  const disconnect = () => {
    logout();
    removeUser();
  };

  return (
    <Main>
      <TranslateButtonHome value={['en', 'fr']} />
      <Span>{`${user.email}`}</Span>
      <Button onClick={addChannel}>
        <FontAwesomeIcon icon={faPlus} />
      </Button>
      <Button onClick={disconnect}>
        <FontAwesomeIcon icon={faRightFromBracket} />
      </Button>
    </Main>
  );
}

export default BottomBar;
