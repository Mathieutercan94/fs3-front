import styled from 'styled-components';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import BottomBar from '../Bars/BottomBar';
import Axios from '../../Axios';

const Main = styled.div`
  display: flex;
  flex-direction: column;
  border-right: 1px solid grey;
  flex: 1;
  min-width: 30vw;
`;

const List = styled.div`
  display: flex;
  flex-direction: column;
`;

const Scrolling = styled.div`
  overflow-y: auto;
  flex: 1;
`;

const Row = styled.div`
  height: 7vh;
  display: flex;
  flex-direction: column;
  min-width: auto;
  background: rgb(36, 37, 38);
  border-top: 1px transparent solid;
  padding: 0 2vw;
  justify-content: space-evenly;
  &:hover {
    cursor: pointer;
    filter: brightness(1.4);
  }
`;

const Title = styled.span`
  height: 5vh;
  padding: 0 1vw;
  display: flex;
  align-items: center;
  border-bottom: 1px solid grey;
`;

const Span = styled.span`
  display: flex;
  align-items: center;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow-x: hidden;
`;

const GreenDot = styled.div`
  width: 10px;
  height: 10px;
  background-color: green;
  border-radius: 50%;
  border-bottom: 1px solid grey;
  margin: 0 10px;
`;
function Channels({ setAllConv, socket }) {
  const { t } = useTranslation();
  const [conv, setConv] = useState([]);
  const [connectedUser, setConnectedUser] = useState([]);
  const [savedData, setSaveData] = useState(null);
  const [newMsg, setNewMsg] = useState(null);
  useEffect(() => {
    if (socket) {
      socket.emit('get_connected', (response) => {
        setConnectedUser(response);
      });
    }
  }, []);

  function loadConv(e) {
    window.location.pathname = `/${e}`;
  }

  function genRow(value) {
    const Rows = value.map((e) => (
      <Row
        key={e.authors[0].id}
        onClick={() => {
          loadConv(e.id);
        }}
      >
        <Span style={{ fontWeight: 'bold' }}>
          {`${e.authors[0].name} ${e.authors[0].surname}`}
          {connectedUser.some((obj) => obj.userid === e.authors[0].id) && <GreenDot />}
        </Span>
        <Span>{e.lastMessage}</Span>
      </Row>
    ));
    return Rows;
  }

  useEffect(() => {
    socket.on('get new conv', (data) => {
      setNewMsg(data);
    });
    if (savedData) {
      savedData.push(newMsg);
      setSaveData([...savedData]);
      setConv(genRow([...savedData]));
    }
  }, [newMsg]);

  useEffect(() => {
    if (socket) {
      socket.on('refresh_connected', (response) => {
        setConnectedUser(response);
      });
    }
    if (savedData) {
      setConv(genRow([...savedData]));
    }
  }, [connectedUser]);

  useEffect(() => {
    Axios.get('conversation/getall').then((res) => {
      setConv(genRow(res.data));
      setSaveData(res.data);
      setAllConv(res.data);
    });
  }, []);

  return (
    <Main>
      <Title>{t('channels')}</Title>
      <Scrolling>
        <List>{[...conv]}</List>
      </Scrolling>
      <BottomBar />
    </Main>
  );
}

export default Channels;
