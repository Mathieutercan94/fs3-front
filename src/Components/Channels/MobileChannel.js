import styled from 'styled-components';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

const Main = styled.button`
  align-self: flex-start;
  width: 5vw;
  height: 100%;
  color: white;
  background: transparent;
  border: 0;
  background: rgb(24, 25, 26);
  &:hover {
    cursor: pointer;
    filter: brightness(2);
  }
`;

function MobileChannel() {
  const moveBackward = () => {
    window.location = '/';
  };

  return window.innerWidth <= 649 ? (
    <Main onClick={moveBackward}>
      <FontAwesomeIcon icon={faArrowLeft} />
    </Main>
  ) : (
    ''
  );
}

export default MobileChannel;
