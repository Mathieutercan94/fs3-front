import styled from 'styled-components';
import io from 'socket.io-client';
import React, { useState } from 'react';
import Chat from '../Components/Messages/Chat';
import Channels from '../Components/Channels/Channels';
import MobileView from '../Components/Mobile/Handler';
import New from '../Components/Messages/New';

const Main = styled.div`
  height: 100%;
  min-height: inherit;
  display: flex;
  color: white;
`;

function Home({ isNew }) {
  const [allConv, setAllConv] = useState([]);
  const user = JSON.parse(window.localStorage.getItem('user'));
  const socket = io('http://44.204.67.99:8081', {
    auth: {
      token: window.localStorage.getItem('jwt')?.split('"')[1],
      id: user.id,
    },
  });

  return (
    <Main>
      <MobileView component={isNew === 'true' ? 'New' : 'Chat'}>
        <Channels socket={socket} setAllConv={setAllConv} />
        {isNew === 'true' ? <New socket={socket} conv={allConv} /> : <Chat socket={socket} />}
      </MobileView>
    </Main>
  );
}

export default Home;
