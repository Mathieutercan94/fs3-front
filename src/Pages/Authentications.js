import styled from 'styled-components';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Login from '../Components/Forms/Login';
import Register from '../Components/Forms/Register';
import { TranslateButtonAuth } from '../Components/Utils/Button';
import { changeLang } from '../i18n';

const Main = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgb(24, 25, 26);
`;

const Form = styled.div`
  height: 50vh;
  width: 35%;
  //background: rgb(36,37,38);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  border-radius: 20px;
  color: white;
  overflow: hidden;
`;

const Tabs = styled.div`
  max-height: 10%;
  width: 100%;
  display: flex;
  flex: 1;
`;

const Tab = styled.span`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  //background: rgb(36,37,38);
  color: white;
  &:hover {
    cursor: pointer;
    filter: brightness(1.4);
  }
`;

function Auth() {
  const tabs = ['login', 'register'];
  const [selectedTab, setSelectedTab] = useState(0);
  const { t } = useTranslation();

  return (
    <Main>
      <Form>
        <Tabs>
          {tabs.map((e, i) => (
            <Tab
              onClick={() => setSelectedTab(i)}
              style={{ borderBottom: selectedTab === i ? '1px solid white' : '0' }}
            >
              {t(e)}
            </Tab>
          ))}
        </Tabs>
        {selectedTab === 0 ? <Login /> : <Register />}
        <div>
          <TranslateButtonAuth
            value='en'
            onClick={(v) => {
              changeLang(v);
            }}
          />
          <TranslateButtonAuth
            value='fr'
            onClick={(v) => {
              changeLang(v);
            }}
          />
        </div>
      </Form>
    </Main>
  );
}

export default Auth;
