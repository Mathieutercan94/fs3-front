import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { i18n } from './i18n';

// import * as serviceWorkerRegistration from './serviceWorkerRegistration';
const root = document.getElementById('root');
i18n.on('initialized', () => {
  ReactDOM.render(<App />, root);
});

// serviceWorkerRegistration.unregister();
