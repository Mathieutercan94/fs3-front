import React, { createContext } from 'react';
import './App.css';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Home from './Pages/Home';
import { Private, Protected } from './Components/Routes/PrivateRoute';
import Auth from './Pages/Authentications';
import { AuthProvider } from './Components/Providers/AuthProvider';
import { UserProvider } from './Components/Providers/UserProvider';

const theme = {
  bg: 'rgb(24,25,26)',
  bg_el: 'rgb(36,37,38)',
  color: 'white',
};

export const ThemeContext = createContext(theme);

function App() {
  return (
    <AuthProvider>
      <UserProvider>
        <div className='App'>
          <BrowserRouter>
            <Routes>
              <Route
                exact
                path='/:id'
                element={
                  <Private>
                    <Home isNew='false' />
                  </Private>
                }
              />
              <Route
                exact
                path='/new'
                element={
                  <Private>
                    <Home isNew='true' />
                  </Private>
                }
              />
              <Route
                exact
                path='/'
                element={
                  <Private>
                    <Home isNew='false' />
                  </Private>
                }
              />
              <Route
                exact
                path='/login'
                element={
                  <Protected>
                    <Auth />
                  </Protected>
                }
              />
              <Route
                path='*'
                element={
                  <Private>
                    <h1>404</h1>
                  </Private>
                }
              />
            </Routes>
          </BrowserRouter>
        </div>
      </UserProvider>
    </AuthProvider>
  );
}

export default App;
