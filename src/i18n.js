import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';

i18n
  .use(initReactI18next)
  .use(Backend)
  .init({
    lng: window.localStorage.getItem('lang') || 'fr',
    fallbackLng: 'fr',
    debug: true,
    initImmediate: false,

    detection: {
      order: ['queryString', 'cookie'],
      cache: ['cookie'],
    },
    interpolation: {
      escapeValue: false,
    },
  });

function changeLang(value) {
  i18n.changeLanguage(value, (err, t) => {
    if (err) {
      console.error('something wrong', err, t('0'));
      return err;
    }
    return null;
  });
  window.localStorage.setItem('lang', value);
  return value;
}

export { i18n, changeLang };
