import axios from 'axios';
import { i18n } from './i18n';

const Axios = axios.create({
  baseURL: 'http://44.204.67.99:8080/api/',
  headers: {
    Common: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${window.localStorage.getItem('jwt')?.split('"')[1]}`,
    },
  },
});
Axios.interceptors.response.use(
  (response) => response,
  (error) => {
    window.alert(i18n.t(error.response.data.code));// eslint-disable-line no-alert
    console.error('debug err axios', error.response.data);
    return Promise.reject(error);
  },
);
export default Axios;
