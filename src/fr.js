exports.fr = {
  0: 'JWT incorrect.',
  1: 'Utilisateur inconnu.',

  2: "Compte banni. Veuillez contacter l'administrateur.",

  3: 'Email déjà utilisé.',

  4: 'Erreur de validation...',

  5: 'Email ou mot de passe incorrect.',

  6: 'Suppression manquante.',

  9: 'URL incorrecte.',

  10: "L'URL est indéfinie.",

  11: 'Le nom est indéfini.',

  13: "L'aéroport manque dans le corps.",

  14: 'Corps incorrect.',

  16: "Cet aéroport n'existe pas.",

  17: "Cet utilisateur n'existe pas.",

  18: 'Erreur interne.',

  19: 'Argument manquant dans le formulaire.',

  20: 'Requête manquante.',

  21: "L'utilisateur n'a pas de données.",

  22: 'UUID introuvable.',

  23: "Le nombre d'utilisateurs doit être supérieur à 1.",

  24: 'Un utilisateur a été ajouté plusieurs fois.',

  25: "Cet utilisateur n'est pas dans cette conversation.",

  26: "Cette requête n'est pas un ID.",

  27: "Ce type n'est pas supporté.",

  28: "La requête n'est pas définie.",
};
